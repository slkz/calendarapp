let translations = {
  'TITLE': {
    'fr': 'Journee a venir',
    'en': 'Day to come'
  },
  'LANGUAGE': {
    'fr': 'Changer la langue par default',
    'en': 'Change default language'
  },

  'MONDAY': {
    'fr': 'Lundi',
    'en': 'Monday'
  },
  'TUESDAY': {
    'fr': 'Mardi',
    'en': 'Tuesday'
  },
  'WEDNESDAY': {
    'fr': 'Mercredi',
    'en': 'Wednesday'
  },
  'THURSDAY': {
    'fr': 'Jeudi',
    'en': 'Thursday'
  },
  'FRIDAY': {
    'fr': 'Vendredi',
    'en': 'Friday'
  },
  'SATURDAY': {
    'fr': 'Samedi',
    'en': 'Saturday'
  },
  'SUNDAY': {
    'fr': 'Dimanche',
    'en': 'Sunday'
  },

  'JANUARY': {
    'fr': 'Janvier',
    'en': 'January'
  },
  'FEBRUARY': {
    'fr': 'Fevrier',
    'en': 'February'
  },
  'MARCH': {
    'fr': 'Mars',
    'en': 'March'
  },
  'APRIL': {
    'fr': 'Avril',
    'en': 'April'
  },
  'MAY': {
    'fr': 'Mai',
    'en': 'May'
  },
  'JUNE': {
    'fr': 'Juin',
    'en': 'June'
  },
  'JULY': {
    'fr': 'Juillet',
    'en': 'July'
  },
  'AUGUST': {
    'fr': 'Aout',
    'en': 'August'
  },
  'SEPTEMBER': {
    'fr': 'Septembre',
    'en': 'September'
  },
  'OCTOBER': {
    'fr': 'Octobre',
    'en': 'October'
  },
  'NOVEMBER': {
    'fr': 'Novembre',
    'en': 'November'
  },
  'DECEMBER': {
    'fr': 'Decembre',
    'en': 'December'
  },

  'SUN' : {
    'fr': 'Ensoleille',
    'en': 'Sunny'
  },
  'SNOW': {
    'fr': 'Neige',
    'en': 'Snow'
  },
  'RAIN': {
    'fr': 'Pluvieux',
    'en': 'Rainy'
  },

  'RAINFALL': {
    'fr': 'Precipitation',
    'en': 'Rainfall'
  },
  'WIND': {
    'fr': 'Vents',
    'en': 'Wind'
  },
  'HUMIDITY': {
    'fr': 'Humidite',
    'en': 'Humidity'
  }
};

function translate(code) {
  const language = localStorage.getItem('LANGUAGE') || 'en';

  return translations[code.toUpperCase()][language];
}

export default { translate };
