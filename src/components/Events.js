import React, { Component } from 'react';
import activitiesTrendIcon from '../views/assets/activities/trend.png';
import activitiesSportIcon from '../views/assets/activities/sport.png';
import activitiesEventIcon from '../views/assets/activities/event.png';


class Events extends Component {
  render() {
    const events = this.props.data[this.props.day];

    return (
      <div className="Events">
        {
          events.map(event => (
            <div className="Event" key={event.id}>
              <img src={event.typeIcon} alt={`${event.type} icon`}/>
              <div>{event.title}</div>
            </div>
          ))
        }
      </div>
    );
  };
}

export default Events;

// == Internal helpers ==============================================

export const FAKE_EVENTS = [
  [
    { id: 0, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
    { id: 1, type: 'sport', typeIcon: activitiesSportIcon, title: 'PSG - LYON' },
    { id: 2, type: 'event', typeIcon: activitiesEventIcon, title: 'Fete des voisins' },
    { id: 3, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
    { id: 4, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
  ],
  [
    { id: 0, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
    { id: 1, type: 'sport', typeIcon: activitiesSportIcon, title: 'PSG - LYON' },
    { id: 2, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
    { id: 3, type: 'event', typeIcon: activitiesEventIcon, title: 'Fete des voisins' },
    { id: 4, type: 'event', typeIcon: activitiesEventIcon, title: 'Fete des voisins' },
    { id: 5, type: 'sport', typeIcon: activitiesSportIcon, title: 'PSG - LYON' },
    { id: 6, type: 'finances', typeIcon: activitiesTrendIcon, title: 'Chute de la livre turque' },
  ],
  [
    { id: 0, type: 'sport', typeIcon: activitiesSportIcon, title: 'PSG - LYON' },
    { id: 1, type: 'event', typeIcon: activitiesEventIcon, title: 'Fete des voisins' },
  ],
  [
  ],
];
