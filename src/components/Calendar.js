import React, { Component } from 'react';
import translations from '../lib/translation';
import languageFrFlag from '../views/assets/flags/languageFrFlag.png';
import languageEnFlag from '../views/assets/flags/languageEnFlag.png';

class Calendar extends Component {
  render() {
    const  { data, selectedDay, selectDay, changeLanguage } = this.props;

    return (
      <div className="calendar">
        <div className="language" onClick={() => changeLanguage()}>
          <img src={localStorage.getItem('LANGUAGE') === 'fr' ? languageEnFlag : languageFrFlag} alt={translations.translate('LANGUAGE')}/>
        </div>
        <div className="calendar-title">{translations.translate('TITLE')}</div>
        <div className="day-list">
          {
            this.props.data.map(day => (
              <div className={day.id === selectedDay ? "day active" : "day"} key={day.id} onClick={() => selectDay(day.id)}>
                <div className="day-name">{translations.translate(day.dayName).substr(0, 3).toLowerCase()}</div>
                <div className="day-number">{day.dayNumber}</div>
              </div>
            ))
          }
        </div>
        <div className="day-full">
          {
            translations.translate(data[selectedDay].dayName) + ' ' +
            data[selectedDay].dayNumber.toString() + ' ' +
            translations.translate(data[selectedDay].month) + ' ' +
            data[selectedDay].year.toString()
          }
        </div>
      </div>
    );
  };
}

export default Calendar;

// == Internal helpers ==============================================

export const FAKE_CALENDAR = [
  { id: 0, dayName: 'monday', dayNumber: 22, month: 'august', year: 2018 },
  { id: 1, dayName: 'tuesday', dayNumber: 23, month: 'august', year: 2018 },
  { id: 2, dayName: 'wednesday', dayNumber: 24, month: 'august', year: 2018 },
  { id: 3, dayName: 'thursday', dayNumber: 25, month: 'august', year: 2018 }
];
