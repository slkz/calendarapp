import React, { Component } from 'react';
import weatherRainIcon from '../views/assets/weather/rain.png';
import weatherSunIcon from '../views/assets/weather/sun.png';
import weatherSnowIcon from '../views/assets/weather/snow.png';
import translations from '../lib/translation';

class Weather extends Component {
  render() {
    const weather = this.props.data[this.props.day];

    return (
      <div className="weather">
        <div className="header">
          <div>
            <div className="temp-current">{weather.currentTemp}°C</div>
            <div className="extremes">
              <div>
                <div>{weather.minTemp}°C</div>
                <div className="text">Tmin</div>
              </div>
              <div>
                <div>{weather.maxTemp}°C</div>
                <div className="text">Tmax</div>
              </div>
            </div>
          </div>
          <div>
            <div>
              <img src={weather.stateIcon} alt='weather.stateIcon'/>
            </div>
            <p>{translations.translate(weather.state)}</p>
          </div>
        </div>
        <div className="stats">
          <div>
            <div className="value">{weather.rainfall}%</div>
            <div className="text">{translations.translate('RAINFALL')}</div>
          </div>
          <div className='middle'>
            <div className="value">{weather.wind}km/h</div>
            <div className="text">{translations.translate('WIND')}</div>
          </div>
          <div>
            <div className="value">{weather.humidity}%</div>
            <div className="text">{translations.translate('HUMIDITY')}</div>
          </div>
        </div>
      </div>
    );
  };
}

export default Weather;

// == Internal helpers ==============================================

export const FAKE_WEATHER = [
  {
    currentTemp: 18,
    minTemp: 15,
    maxTemp: 25,
    state: 'Rain',
    stateIcon: weatherRainIcon,
    rainfall: 87,
    wind: 21,
    humidity: 67
  },
  {
    minTemp: 12,
    maxTemp: 23,
    state: 'Sun',
    stateIcon: weatherSunIcon,
    rainfall: 10,
    wind: 9,
    humidity: 69
  },
  {
    minTemp: 7,
    maxTemp: 20,
    state: 'Sun',
    stateIcon: weatherSunIcon,
    rainfall: 10,
    wind: 15,
    humidity: 58
  },
  {
    minTemp: -2,
    maxTemp: 3,
    state: 'Snow',
    stateIcon: weatherSnowIcon,
    rainfall: 0,
    wind: 21,
    humidity: 48
  },
  {
    minTemp: 25,
    maxTemp: 21,
    state: 'Rain',
    stateIcon: weatherRainIcon,
    rainfall: 90,
    wind: 21,
    humidity: 67
  }
];
