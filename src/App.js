import React, { Component } from 'react';
import Calendar, { FAKE_CALENDAR } from './components/Calendar';
import Weather, { FAKE_WEATHER } from './components/Weather';
import Events, { FAKE_EVENTS } from './components/Events';


class App extends Component {
  state = {
    selectedDay: 0,
    language: 'en'
  };

  selectDay = (index) => {
    this.setState({selectedDay: index});
  };

  changeLanguage = () => {
    if (this.state.language === 'fr') {
      this.setState({language: 'en'});
      localStorage.setItem('LANGUAGE', 'en');
      return ;
    }
    this.setState({language: 'fr'});
    localStorage.setItem('LANGUAGE', 'fr');
  };

  render() {
    return (
      <div className="App">
        {/*replace FAKE_CALENDAR by function*/}
        < Calendar data={FAKE_CALENDAR} selectedDay={this.state.selectedDay} selectDay={this.selectDay} changeLanguage={this.changeLanguage}/>
        < Weather data={FAKE_WEATHER} day={this.state.selectedDay}/>
        < Events data={FAKE_EVENTS} day={this.state.selectedDay}/>
      </div>
    );
  }
}

export default App;
